# Interpreter wyrażeń algebraicznych
Konrad Mrozowski,
Dominik Bryś
## Możliwości
- Obliczanie wyrażeń (np. `2+2*2`)
- Obliczanie nierówności (np. `2+2 > 1+2`)
- Możliwość definiowania zmiennych (np. `x:=2`)
- Możliwość odczytu zmiennych (np. `x+2`)
- Funkcje jednoargumentowe (`sin`,`cos`,`tan`,`asin`, `acos`, `atan`,
`log`,`ln`,`sqrt`)
- Funkcje wieloargumentowe (`avg`)

# Budowanie i uruchamianie programu
##Warunki wstępne
- Żeby móc uruchomić program należy mieć zainstalowaną Javę w wersji conajmniej 8
- Żeby móc zbudować program należy mieć zainstalowany program `gradle`

## Budowanie i Testowanie
`gradle build` wykona budowanie, linkowanie oraz uruchamia testy.
Wyniki testów znajdują się w pliku `build/reports/tests/test/index.html`

##Użycie
Żeby uruchomić interpreter korzystając z gradle: `gradle run -q --console=plain`
Zostaniemy przekierowani do interfejsu typu REPL (Read-Eval-Print Loop) w którym możemy wprowadzać wyrażenia.
Żeby zakończyć działanie programu należy wcisnąć Ctrl+Z

# Struktura kodu i sposób działania
## Antlr i Java
W pliku `build.gradle` znajduje się wywołania plugina odpowiedzialnego za antlr. Bierze on plik gramatyki 
`Calculator.g4` z folderu `src/main/antlr` i na jego podstawie generuje pliki Javy. Klasa `Evaluator` korzysta z 
wygenerowanego w ten sposób leksera i parsera. Po zmianie stringa na drzewo wyrażeń jest ono przeliczane poprzez klasę
`InterpreterNodeVisitor` działającą w taki sposób, że dla każdego węzła drzewa jest przeliczany wynik cześciowy.  Wyniki są
opakowane w klasę `EvaluatorResult` reprezentujacą wynik (Wynik może być wynikiem wyrażenia, nierówności, przepisania, etc.)
Na przykład dla węzłów terminalnych (liczb, stałych, zmiennych, funckcje) zwracany jest wynik w postaci wartości tych liczb (typ BigDecimal),
dla wyrażeń są przeliczane węzły terminalne następnie są wywoływane na nich operatory (+,-,*,/). Po dojściu do korzenia drzewa
cały wynik jest zwracany jako wynik całego wprowadzonego przez użytkownika drzewa. Żeby wiadomo było jakie dostępne funkcje i zmienne
mamy w systemie dostępne są klasy `VariableContext` i `FunctionContext`.
## Opis gramatyki i parsera
### Root
Korzeniem drzewa dopasowań jest identyfikator `root`. Reprezentuje on wszystkie operacje dostępne w systemie.
Na chwile obecną jest to równanie(`equation`) i przypisanie do zmiennej(`assignment`).
### Równanie
Równanie może się składać z wyrażenia(`expression`) oraz opcjonalnego drugiego wyrażenia i operacji relacyjnej (<,>,=) między nimi.
Jeżeli nie ma drugiego składnika równania wynik zwracany jest jako wynik wyrażenia (`expression`). Inaczej jest zwracany wynik równania (true/false)
### Wyrażenie
Wyrażenia są reprezentowane przez identyfikator `expression`.
Kolejność działania na operatorach (najpierw potęga, potem mnożenie/dzielenie potem dodawanie/odejmowanie) jest narzucone przez podzielenie
wyrażeń na "wyrażenia mnożące" (`multiplyingExpression`) oraz "wyrażenia potęgujące" (`powerExpression`)
Identyfikator `expression` jest sumą/różnicą co najmniej jednego wyrażenia mnożącego.
Identyfikator `multiplyingExpression` jest iloczynem/ilorazem co najmniej jednego wyrażenia potęgującego
Podobnie rzecz się ma z identyfikatorem `powerExpression`. "Co najmniej jednego" jest warunkiem pozwalającym 
poprawnie parsować wyrażenia, które nie zawierają którejś z operacji. Gdyby np. `multiplyingExpression` musiało mieć dwa, 
to system nie parsowałby wyrażeń nie mających w sobie mnożenia.

### Atomy
Wyrażenia atomowe (to jest wywołania funkcji, stałe, zmienne, liczby) są reprezentowane identyfikatorem `signedAtom`
Może być albo funkcją `func` albo typem `atom` albo kolejnym identyfikatorem `signedAtom` ze znakiem `+` i `-`.
Rekurencja taka została zrobiona, żeby parser prawidłowo parsował wyrażenia takie jak `---3`
Sam zaś `atom` może być liczbą `scientific` o notacji naukowej, zmienną `variable`, stałą `constant` lub wyrażeniem w nawiasach

## Testy
Testy zostały napisane w języku Groovy, korzystając z frameworka spock.
