package pl.agh.wiet.interpreter.evaluator.context

import pl.agh.wiet.interpreter.evaluator.exceptions.ConstantRedefinitionException
import pl.agh.wiet.interpreter.evaluator.exceptions.VariableNotDefinedException
import spock.lang.Specification

class VariableContextSpec extends Specification {

    VariableContext variableContext;

    def setup() {
        variableContext = new VariableContext();
    }
    def "Should contain 'e' and 'pi' constant"() {
        when:
        def result = variableContext.getConstants()
        then:
        result.contains("pi")
        result.contains("e")
    }

    def "Should get Pi constant"() {
        when:
        def result = variableContext.get("pi")
        then:
        result == 3.141592653589793
    }

    def "Should assign variable"() {
        when:
        variableContext.assign("x", 1.3)
        then:
        variableContext.get("x") == 1.3
    }

    def "Should assign variable case-insensitive"() {
        when:
        variableContext.assign("X", 1.3)
        then:
        variableContext.get("x") == 1.3
    }

    def "Should assign constant"() {
        when:
        variableContext.assignConstant("x", 1.3)
        then:
        variableContext.get("x") == 1.3
        variableContext.constants.contains("x")
    }

    def "Should assign constant case-insensitive"() {
        when:
        variableContext.assignConstant("X", 1.3)
        then:
        variableContext.get("x") == 1.3
        variableContext.constants.contains("x")
    }

    def "Shouldn't reassign constant"() {
        when:
        variableContext.assign("pi", 1.3)
        then:
        thrown ConstantRedefinitionException
    }

    def "Should unassign variable"() {
        given:
        variableContext.assign("x", 1.3)
        when:
        variableContext.unassign("x")
        variableContext.get("x")
        then:
        thrown VariableNotDefinedException
    }

    def "Should get all available names"() {
        given:
        variableContext.assign("x", 1.3)
        variableContext.assignConstant("y", 1.4)
        when:
        def result = variableContext.all
        then:
        result.contains("x")
        result.contains("y")
    }

    def "Should get all available variables"() {
        given:
        variableContext.assign("x", 1.3)
        variableContext.assignConstant("y", 1.4)
        when:
        def result = variableContext.variables
        then:
        result.contains("x")
        !result.contains("y")
    }

    def "Should get all available constants"() {
        given:
        variableContext.assign("x", 1.3)
        variableContext.assignConstant("y", 1.4)
        when:
        def result = variableContext.constants
        then:
        !result.contains("x")
        result.contains("y")
    }
}
