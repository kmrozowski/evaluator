package pl.agh.wiet.interpreter.evaluator

import pl.agh.wiet.interpreter.evaluator.context.FunctionContext
import pl.agh.wiet.interpreter.evaluator.context.VariableContext
import spock.lang.Specification
import spock.lang.Unroll

class EvaluatorSpec extends Specification {
    FunctionContext functionContext;
    VariableContext variableContext;
    Evaluator evaluator;

    def setup() {
        functionContext = new FunctionContext();
        variableContext = new VariableContext();
        evaluator = new Evaluator(functionContext, variableContext);
    }

    @Unroll
    def "Should evaluate number #x = #y"() {
        when:
        def result = evaluator.evaluate(x);
        then:
        result.type == EvaluatorResultType.EXPRESSION
        result.expressionValue == y
        where:
        x          |  y
        "2"        |  2.0
        "-2"       | -2.0
        "2.0"      |  2.0
        "0.5"      |  0.5
        "--0.5"    |  0.5
        "-+--++0.5"| -0.5
        //TODO ".5" -> 0.5 ?
    }

    @Unroll
    def "Should evaluate costant #x = #y"() {
        when:
        def result = evaluator.evaluate(x);
        then:
        result.type == EvaluatorResultType.EXPRESSION
        result.expressionValue == y
        where:
        x    | y
        "pi" | 3.141592653589793
        "e"  | 2.718281828459045
    }

    @Unroll
    def "Should evaluate function call #x = #y" () {
        when:
        def result = evaluator.evaluate(x);
        then:
        result.type == EvaluatorResultType.EXPRESSION
        result.expressionValue == y
        where:
        x         |  y
        "sin(1)"             | 0.8414709848078965
        "sin(1+1)"           | 0.9092974268256817
        "sin(1 + sin(1))"    | 0.9635907245418334
        "avg(1,2,3)"         | 2.0
        "avg(1+2,sin(1),2*2)"| 2.6138236616026322
    }

    @Unroll
    def "Should evaluate expression #x = #y"() {
        when:
        def result = evaluator.evaluate(x);
        then:
        result.type == EvaluatorResultType.EXPRESSION
        result.expressionValue == y
        where:
        x         |  y
        "2+2"     |  4.0
        "2+2+2"   |  6.0
        "2-2"     |  0.0
        "2-2-2"   | -2.0
        "2*2"     |  4.0
        "2*2*2"   |  8.0
        "2/2"     |  1.0
        "2/2/2"   |  0.5
        "2^2"     |  4.0
        "2^2^2"   |  16.0
        "2+2*2"   |  6.0
        "2+2*2^2" |  10.0
        "(2+2)*2" |  8.0
        "pi+2"    |  5.141592653589793
    }

    @Unroll
    def "Should evaluate equation #x = #y" () {
        when:
        def result = evaluator.evaluate(x);
        then:
        result.type == EvaluatorResultType.EQUATION
        result.equationValue == y
        where:
        x       |  y
        "2>2"   | false
        "2=2"   | true
        "2>2"   | false
    }

    def "Should evaluate variable"() {
        given:
        variableContext.assign("x",3.5)
        when:
        def result = evaluator.evaluate("x")
        then:
        result.type == EvaluatorResultType.EXPRESSION
        result.expressionValue == 3.5
    }

    def "Should assign variable"() {
        when:
        def result = evaluator.evaluate("y:=5")
        then:
        result.type == EvaluatorResultType.ASSIGNMENT
        variableContext.get("y") == 5.0
    }

}
