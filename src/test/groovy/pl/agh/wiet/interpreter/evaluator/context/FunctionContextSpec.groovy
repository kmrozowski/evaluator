package pl.agh.wiet.interpreter.evaluator.context

import spock.lang.Specification
import spock.lang.Unroll

class FunctionContextSpec extends Specification {
    FunctionContext functionContext;

    def setup() {
        functionContext = new FunctionContext()
    }

    def "Should contain all standard functions"() {
        when:
        def result = functionContext.all;
        then:
        result.contains("sin")
        result.contains("cos")
        result.contains("tan")
        result.contains("cotan")
        result.contains("asin")
        result.contains("acos")
        result.contains("atan")
        result.contains("ln")
        result.contains("log")
        result.contains("sqrt")
        result.contains("rand")
        result.contains("avg")
    }

    @Unroll
    def "Should evaluate deterministic functions"(String fun, BigDecimal[] args, BigDecimal val) {
        expect:
        functionContext.evaluate(fun, args) == val

        where:
        fun   | args    | val
        "sin" | [1]     | 0.8414709848078965
        "cos" | [1]     | 0.5403023058681398
        "avg" | [1,2,3] | 2
        "ln"  | [10]    | 2.302585092994046
    }
}
