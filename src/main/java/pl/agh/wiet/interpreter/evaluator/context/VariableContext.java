package pl.agh.wiet.interpreter.evaluator.context;

import pl.agh.wiet.interpreter.evaluator.exceptions.ConstantRedefinitionException;
import pl.agh.wiet.interpreter.evaluator.exceptions.VariableNotDefinedException;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.*;

/**
 * Class holding available variables and constants. Comes with 'e' and 'pi' constants.
 */
public class VariableContext {
    private final Map<String, BigDecimal> variables = new HashMap<String, BigDecimal> () {{
        put("pi", BigDecimal.valueOf(Math.PI));
        put("e", BigDecimal.valueOf(Math.E));
    }};

    private final Set<String> constants = new HashSet<String>() {{
        add("pi");
        add("e");
    }};

    /**
     * Adds variable to context. Throws ConstantRedefinitionException if variable has the same name as one of the
     * constants defined in a session.
     * @param variable
     * @param value
     */
    public void assign(@NotEmpty String variable, @NotNull BigDecimal value) {
        if(constants.contains(variable.toLowerCase())) {
            throw new ConstantRedefinitionException(variable);
        }
        variables.put(variable.toLowerCase(), value);
    }

    /**
     * Adds constants to context. Throws ConstantRedefinitionException if it has the same name as one of the
     * constants defined in a session.
     * @param variable
     * @param value
     */
    public void assignConstant(@NotEmpty String variable, @NotNull BigDecimal value) {
        assign(variable,value);
        constants.add(variable.toLowerCase());
    }

    /**
     * Gets value of the variable with specified name. Throws VariableNotDefined if it's not in the context.
     * @param variable
     * @return
     */
    public BigDecimal get(@NotEmpty String variable) {
        if(!variables.keySet().contains(variable.toLowerCase())) {
            throw new VariableNotDefinedException(variable);
        }
        return variables.get(variable.toLowerCase());
    }

    /**
     * Removes variable from the context.
     * @param variable
     */
    public void unassign(@NotEmpty String variable) {
        variables.remove(variable.toLowerCase());
    }

    /**
     * Gets names of all variables and constants available in the context.
     * @return
     */
    public Set<String> getAll() {
        return variables.keySet();
    }

    /**
     * Gets names of all variables avaiable in the context.
     * @return
     */
    public Set<String> getVariables() {
        Set<String> onlyVariables = variables.keySet();
        onlyVariables.removeAll(constants);
        return onlyVariables;
    }

    /**
     * Gets names of all constants available in the context.
     * @return
     */
    public Set<String> getConstants() {
        return constants;
    }
}
