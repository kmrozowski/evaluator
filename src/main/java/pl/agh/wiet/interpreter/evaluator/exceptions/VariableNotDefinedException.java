package pl.agh.wiet.interpreter.evaluator.exceptions;

public class VariableNotDefinedException extends RuntimeException {
    private String variableName;

    public VariableNotDefinedException(String variableName) {
        super();
        this.variableName = variableName;
    }

    public String getVariableName() {
        return variableName;
    }
}
