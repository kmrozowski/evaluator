package pl.agh.wiet.interpreter.evaluator.visitor;

import pl.agh.wiet.interpreter.evaluator.context.FunctionContext;
import pl.agh.wiet.interpreter.evaluator.context.VariableContext;
import pl.agh.wiet.interpreter.evaluator.EvaluatorResult;
import pl.agh.wiet.interpreter.grammar.CalculatorBaseVisitor;
import pl.agh.wiet.interpreter.grammar.CalculatorParser;


import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import pl.agh.wiet.interpreter.grammar.CalculatorParser.ScientificContext;

/**
 * Parse tree visitor. Evaulates parse tree.
 */
public class InterpreterNodeVisitor extends CalculatorBaseVisitor<EvaluatorResult> {

    private FunctionContext functionContext;
    private VariableContext variableContext;

    public InterpreterNodeVisitor(FunctionContext functionContext, VariableContext variableContext) {
        this.functionContext = functionContext;
        this.variableContext = variableContext;
    }

    /**
     * Visits scientific number. Creates BigDecimal instance from string.
     * @param ctx
     * @return
     */
    @Override
    public EvaluatorResult visitScientific(ScientificContext ctx) {
        BigDecimal val = new BigDecimal(ctx.getText());
        return EvaluatorResult.expressionResult(val);
    }

    /**
     * Visits variable. Gets it's value as a BigDecimal if it exists, throws VariableNotDefinedException if it doesn't
     * @param ctx
     * @return
     */
    @Override
    public EvaluatorResult visitVariable(pl.agh.wiet.interpreter.grammar.CalculatorParser.VariableContext ctx) {
        BigDecimal val = variableContext.get(ctx.getText());
        return EvaluatorResult.expressionResult(val);
    }

    /**
     * TODO remove constant from grammar and replace it with variable
     * @param ctx
     * @return
     */
    @Override
    public EvaluatorResult visitConstant(pl.agh.wiet.interpreter.grammar.CalculatorParser.ConstantContext ctx) {
        BigDecimal val = variableContext.get(ctx.getText());
        return EvaluatorResult.expressionResult(val);
    }

    @Override
    public EvaluatorResult visitAtom(pl.agh.wiet.interpreter.grammar.CalculatorParser.AtomContext ctx) {
       if(ctx.scientific() != null) {
           return visitScientific(ctx.scientific());
       } else if (ctx.variable() != null) {
           return visitVariable(ctx.variable());
       } else if (ctx.constant() != null) {
           return visitConstant(ctx.constant());
       } else {
           return visitExpression(ctx.expression());
       }
    }

    @Override
    public EvaluatorResult visitSignedAtom(pl.agh.wiet.interpreter.grammar.CalculatorParser.SignedAtomContext ctx) {
       if (ctx.operation != null) {
           if(ctx.operation.getType() == CalculatorParser.MINUS) {
               EvaluatorResult result = visitSignedAtom(ctx.signedAtom());
               return EvaluatorResult.expressionResult(result.getExpressionValue().negate());
           } else {
               return visitSignedAtom(ctx.signedAtom());
           }
       }
       if(ctx.func() != null) {
           return visitFunc(ctx.func());
       } else {
           return visitAtom(ctx.atom());
       }
    }

    @Override
    public EvaluatorResult visitFunc(CalculatorParser.FuncContext ctx) {
        String funcname = ctx.funcname().getText();
        List<EvaluatorResult> argExpressions =  ctx.expression().stream()
                .map(e -> visitExpression(e))
                .collect(Collectors.toList());

        BigDecimal[] args = argExpressions.stream()
                .map(r -> r.getExpressionValue())
                .toArray(BigDecimal[]::new);

        BigDecimal val = functionContext.evaluate(funcname, args);
        return EvaluatorResult.expressionResult(val);
    }

    @Override
    public EvaluatorResult visitPowerExpression(CalculatorParser.PowerExpressionContext ctx) {
        List<CalculatorParser.SignedAtomContext> signedAtoms = ctx.signedAtom();
        EvaluatorResult left = visitSignedAtom(signedAtoms.get(0));
        if(signedAtoms.size() > 1) {
            BigDecimal value = left.getExpressionValue();
            for(int i = 1; i < signedAtoms.size(); i++) {
                BigDecimal next = visitSignedAtom(signedAtoms.get(i)).getExpressionValue();
                value = BigDecimal.valueOf(Math.pow(value.doubleValue(), next.doubleValue()));
            }
            return EvaluatorResult.expressionResult(value);
        } else {
            return left;
        }
    }

    @Override
    public EvaluatorResult visitMultiplyingExpression(CalculatorParser.MultiplyingExpressionContext ctx) {
        List<CalculatorParser.PowerExpressionContext> powerExpressions = ctx.powerExpression();
        EvaluatorResult left = visitPowerExpression(powerExpressions.get(0));
        if(powerExpressions.size() > 1) {
            BigDecimal value = left.getExpressionValue();
            for(int i = 1; i < powerExpressions.size(); i++) {
                BigDecimal next = visitPowerExpression(powerExpressions.get(i)).getExpressionValue();
                if(ctx.TIMES(i - 1) != null) {
                    value = value.multiply(next);
                } else {
                    value = value.divide(next);
                }
            }
            return EvaluatorResult.expressionResult(value);
        } else {
            return left;
        }
    }

    @Override
    public EvaluatorResult visitExpression(CalculatorParser.ExpressionContext ctx) {
        List<CalculatorParser.MultiplyingExpressionContext> multiplyingExpressions = ctx.multiplyingExpression();
        EvaluatorResult left = visitMultiplyingExpression(multiplyingExpressions.get(0));
        if(multiplyingExpressions.size() > 1) {
            BigDecimal value = left.getExpressionValue();
            for(int i = 1; i < multiplyingExpressions.size(); i++) {
                BigDecimal next = visitMultiplyingExpression(multiplyingExpressions.get(i)).getExpressionValue();
                if(ctx.PLUS(i - 1) != null) {
                    value = value.add(next);
                } else {
                    value = value.subtract(next);
                }
            }
            return EvaluatorResult.expressionResult(value);
        } else {
            return left;
        }
    }

    @Override
    public EvaluatorResult visitEquation(CalculatorParser.EquationContext ctx) {
        EvaluatorResult left = visitExpression(ctx.expression(0));
        if(ctx.expression(1) != null) {
            EvaluatorResult right = visitExpression(ctx.expression(1));
            boolean val;
            int result = left.getExpressionValue().compareTo(right.getExpressionValue());

            if(ctx.relop().GT() != null) {
                val = result > 0;
            } else if(ctx.relop().LT() != null) {
                val = left.getExpressionValue().compareTo(right.getExpressionValue()) < 0;
            } else {
                val = left.getExpressionValue().compareTo(right.getExpressionValue()) == 0;
            }

            return EvaluatorResult.equationResult(val);
        } else {
            return left;
        }
    }

    @Override
    public EvaluatorResult visitAssignment(CalculatorParser.AssignmentContext ctx) {
        String variableName = ctx.variable().getText();
        BigDecimal value = visitExpression(ctx.expression()).getExpressionValue();
        variableContext.assign(variableName, value);
        return EvaluatorResult.assignmentResult(variableName);
    }

    @Override
    public EvaluatorResult visitRoot(CalculatorParser.RootContext ctx) {
        if(ctx.assignment() != null) {
            return visitAssignment(ctx.assignment());
        } else {
            return visitEquation(ctx.equation());
        }
    }
}
