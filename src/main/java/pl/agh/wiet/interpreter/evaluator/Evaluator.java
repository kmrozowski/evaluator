package pl.agh.wiet.interpreter.evaluator;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import pl.agh.wiet.interpreter.evaluator.context.FunctionContext;
import pl.agh.wiet.interpreter.evaluator.context.VariableContext;
import pl.agh.wiet.interpreter.evaluator.visitor.InterpreterNodeVisitor;
import pl.agh.wiet.interpreter.grammar.CalculatorLexer;
import pl.agh.wiet.interpreter.grammar.CalculatorParser;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Evaluator.
 */
public class Evaluator {
    private final FunctionContext functionContext;
    private final VariableContext variableContext;

    private InterpreterNodeVisitor visitor;

    public Evaluator(@NotNull FunctionContext functionContext, @NotNull VariableContext variableContext) {
        this.functionContext = functionContext;
        this.variableContext = variableContext;
        this.visitor = new InterpreterNodeVisitor(functionContext, variableContext);
    }

    /**
     * Evaluate string
     * @param input
     * @return
     */
    public EvaluatorResult evaluate(@NotEmpty String input) {
        CalculatorLexer lexer = new CalculatorLexer(CharStreams.fromString(input));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CalculatorParser parser = new CalculatorParser(tokens);
        ParseTree tree = parser.root();
        return visitor.visit(tree);
    }

}
