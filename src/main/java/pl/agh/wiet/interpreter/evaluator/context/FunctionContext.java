package pl.agh.wiet.interpreter.evaluator.context;

import pl.agh.wiet.interpreter.evaluator.exceptions.FunctionNotDefinedException;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

public class FunctionContext {

    private final Map<String, Function<BigDecimal[], Double>> functions = new HashMap<>();
    public FunctionContext() {
        functions.put("sin", arg -> Math.sin(arg[0].doubleValue()));
        functions.put("cos", arg -> Math.cos(arg[0].doubleValue()));
        functions.put("tan", arg -> Math.tan(arg[0].doubleValue()));
        functions.put("cotan", arg -> 1.0 / Math.tan(arg[0].doubleValue()));
        functions.put("asin", arg -> Math.asin(arg[0].doubleValue()));
        functions.put("acos", arg -> Math.acos(arg[0].doubleValue()));
        functions.put("atan", arg -> Math.atan(arg[0].doubleValue()));
        functions.put("ln", arg -> Math.log(arg[0].doubleValue()));
        functions.put("log", arg -> Math.log(arg[1].doubleValue()) / Math.log(arg[0].doubleValue()));
        functions.put("sqrt", arg -> Math.sqrt(arg[0].doubleValue()));
        functions.put("rand", arg -> Math.random());
        functions.put("avg", arg -> Stream.of(arg).mapToDouble(a -> a.doubleValue()).average().orElse(Double.NaN));
    }

    public BigDecimal evaluate(@NotEmpty String funcName) {
        return evaluate(funcName, new BigDecimal[]{});
    }

    public BigDecimal evaluate(@NotEmpty String funcName, @NotNull BigDecimal[] arguments) {
        return Optional.ofNullable(functions.get(funcName))
                .map(f -> f.apply(arguments))
                .map(BigDecimal::valueOf)
                .orElseThrow(() -> new FunctionNotDefinedException(funcName));
    }

    public Set<String> getAll() {
        return functions.keySet();
    }
}
