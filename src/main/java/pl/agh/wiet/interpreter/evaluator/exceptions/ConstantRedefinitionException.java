package pl.agh.wiet.interpreter.evaluator.exceptions;

public class ConstantRedefinitionException extends RuntimeException {
    private String constantName;

    public ConstantRedefinitionException(String constantName) {
        super();
        this.constantName = constantName;
    }

    public String getConstantName() {
        return constantName;
    }
}
