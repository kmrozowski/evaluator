package pl.agh.wiet.interpreter.evaluator;

import java.math.BigDecimal;

/**
 * Evaluator result.
 */
public class EvaluatorResult {
    /**
     * Result type
     */
    private EvaluatorResultType type;
    /**
     * Expression value
     */
    private BigDecimal expressionValue;
    /**
     * Equation value
     */
    private Boolean equationValue;

    /**
     * Variable name
     */
    private String variableName;

    public static EvaluatorResult expressionResult(BigDecimal value) {
        return new EvaluatorResult(EvaluatorResultType.EXPRESSION, value, null, null);
    }

    public static EvaluatorResult equationResult(Boolean value) {
        return new EvaluatorResult(EvaluatorResultType.EQUATION, null, value, null);
    }

    public static EvaluatorResult assignmentResult(String variableName) {
        return new EvaluatorResult(EvaluatorResultType.ASSIGNMENT, null, null, variableName);
    }

    public EvaluatorResultType getType() {
        return type;
    }

    public BigDecimal getExpressionValue() {
        return expressionValue;
    }

    public Boolean getEquationValue() {
        return equationValue;
    }


    private EvaluatorResult(EvaluatorResultType type, BigDecimal expressionValue, Boolean equationValue, String variableName) {
        this.type = type;
        this.expressionValue = expressionValue;
        this.equationValue = equationValue;
        this.variableName = variableName;
    }
}
