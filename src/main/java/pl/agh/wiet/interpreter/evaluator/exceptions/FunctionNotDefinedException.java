package pl.agh.wiet.interpreter.evaluator.exceptions;

public class FunctionNotDefinedException extends RuntimeException {
    private String functionName;

    public FunctionNotDefinedException(String functionName) {
        super();
        this.functionName = functionName;
    }

    public String getFunctionName() {
        return functionName;
    }
}
