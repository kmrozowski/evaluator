package pl.agh.wiet.interpreter.evaluator;

/**
 * Evaluator result type.
 */
public enum EvaluatorResultType {
    /**
     * Expression. For example: "2+2*2"
     */
    EXPRESSION,
    /**
     * Equation. For example "2<3"
     */
    EQUATION,
    /**
     * Assignment. For example "x=2"
     */
    ASSIGNMENT
}
