package pl.agh.wiet.interpreter;

import pl.agh.wiet.interpreter.frontend.EvaluatorREPLInterface;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        EvaluatorREPLInterface replInterface = new EvaluatorREPLInterface();
        replInterface.processInput();
    }
}
