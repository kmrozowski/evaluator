package pl.agh.wiet.interpreter.frontend.formatter;

import pl.agh.wiet.interpreter.evaluator.EvaluatorResult;

public class EvaluatorResultFormatter {

    public String format(EvaluatorResult result) {
        switch (result.getType()) {
            case EQUATION:
                return "Equation value = " + result.getEquationValue();
            case ASSIGNMENT:
                return "Assignment successful";
            case EXPRESSION:
                return "Expression value = " + result.getExpressionValue();
            default:
                return "Unknown evaluator result";
        }
    }
}
