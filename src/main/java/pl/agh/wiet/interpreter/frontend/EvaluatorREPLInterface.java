package pl.agh.wiet.interpreter.frontend;

import pl.agh.wiet.interpreter.evaluator.Evaluator;
import pl.agh.wiet.interpreter.evaluator.EvaluatorResult;
import pl.agh.wiet.interpreter.evaluator.context.FunctionContext;
import pl.agh.wiet.interpreter.evaluator.context.VariableContext;
import pl.agh.wiet.interpreter.frontend.formatter.EvaluatorResultFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EvaluatorREPLInterface {

    private EvaluatorResultFormatter formatter = new EvaluatorResultFormatter();
    private FunctionContext functionContext = new FunctionContext();
    private VariableContext variableContext = new VariableContext();

    private Evaluator evaluator = new Evaluator(functionContext, variableContext);

    public void processInput() throws IOException {
        System.out.println("Evaluator ver. 0.5. Using command line mode.");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        System.out.print(" >");
        while((input = reader.readLine()) != null) {
            if(!input.isEmpty()) {
                EvaluatorResult result = evaluator.evaluate(input);
                System.out.println(formatter.format(result));
            }
            System.out.print(" >");
        }
        System.out.println("Goodbye :)");
    }
}
